package ma.octo.assignement.web;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.dto.UtilisateurDto;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.repository.VirementRepository;
import ma.octo.assignement.service.AutiService;
import ma.octo.assignement.service.IntServices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@RestController

@RequestMapping(value = "/bank")
class BankController {

    public static final int MONTANT_MAXIMAL = 10000;

    Logger LOGGER = LoggerFactory.getLogger(BankController.class);

    /*@Autowired
    private CompteRepository rep1;
    @Autowired
    private VirementRepository re2;
    @Autowired
    private AutiService monservice;
    @Autowired
    private UtilisateurRepository re3;*/
    @Autowired
    private IntServices services;
    
    public BankController(IntServices services) {
    	
		this.services = services;
	}

    @GetMapping("virements")
    List<VirementDto> loadAllVirements() {
        return services.loadVirements();
    }
	
	@GetMapping("versements")
	List<VersementDto> loadAllVersements(){
		return services.loadVersements();
	}

	@GetMapping("comptes")
    List<Compte> loadAllCompte() {
       return services.loadComptes();
    }

    @GetMapping("utilisateurs")
    List<Utilisateur> loadAllUtilisateur() {
        return services.loadUtilisateurs();
    }
    
    @PostMapping("/creeCompte")
    @ResponseStatus(HttpStatus.CREATED)
    public void creeCompte(@RequestBody CompteDto compteDto) {
    	
        services.initCompte(compteDto);
    }
    @PostMapping("/creeUtilisateur")
    @ResponseStatus(HttpStatus.CREATED)
    public void creeCompte(@RequestBody UtilisateurDto utilisateurDto) {
    	
        services.initUtilisteur(utilisateurDto);
    }

    @PostMapping("/doVirement")
    @ResponseStatus(HttpStatus.CREATED)
    public void createVirement(@RequestBody VirementDto virementDto)
            throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
    	
        services.newVirement(virementDto);
    }

    @PostMapping("/doVersement")
    @ResponseStatus(HttpStatus.CREATED)
	public void createVersement(@RequestBody VersementDto versementDto)
			throws CompteNonExistantException, TransactionException, SoldeDisponibleInsuffisantException {
    	services.newVersement(versementDto);
    }
}
