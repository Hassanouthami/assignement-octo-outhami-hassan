package ma.octo.assignement.dto;

import java.math.BigDecimal;
import java.util.Date;
import lombok.NoArgsConstructor;



import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import lombok.Data;
import ma.octo.assignement.domain.Utilisateur;
import lombok.AllArgsConstructor;
@NoArgsConstructor
@AllArgsConstructor
@Data
public class CompteDto {
	  private String nrCompte;
	  private String rib;
	  private String username;
	  private BigDecimal solde;
	  
}
