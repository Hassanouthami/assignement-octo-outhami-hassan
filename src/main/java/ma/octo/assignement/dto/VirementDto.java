package ma.octo.assignement.dto;

import java.math.BigDecimal;
import lombok.Data;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class VirementDto {
  private String nrCompteEmetteur;
  private String nrCompteBeneficiaire;
  private String motifVirement;
  private BigDecimal montantVirement;
  private Date dateVirement;
}
