package ma.octo.assignement.dto;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class UtilisateurDto {
	 private String username;
	 private String lastname;
	 private String firstname;
	 private String gender;
	 private Date birthdate;
}
