package ma.octo.assignement.dto;

import java.math.BigDecimal;
import lombok.Data;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class VersementDto {
	  private String usernameEmetteur;
	  private String nrCompteBeneficiaire;
	  private String motifVersement;
	  private BigDecimal montantVersement;
	  private Date dateVersement;
}
