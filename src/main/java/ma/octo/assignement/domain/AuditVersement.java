package ma.octo.assignement.domain;

import ma.octo.assignement.domain.util.EventType;

import java.io.Serializable;

import javax.persistence.*;
import lombok.Data;
@Entity
@Data
@Table(name = "AUDIT_VIREMENT")
public class AuditVersement  implements Serializable {
  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

@Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(length = 100)
  private String message;

  @Enumerated(EnumType.STRING)
  private EventType eventType;

  
}
