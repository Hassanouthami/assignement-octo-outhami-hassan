package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.UtilisateurDto;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;

public class VirementMapper {

    private static VirementDto virementDto;
    private static VersementDto versementDto;
    //private static Utilisateur utilisateur;

    public static VirementDto virementToVirementDto(Virement virement) {
        virementDto = new VirementDto();
        virementDto.setNrCompteEmetteur(virement.getCompteEmetteur().getNrCompte());
        virementDto.setNrCompteBeneficiaire(virement.getCompteBeneficiaire().getNrCompte());
        virementDto.setMotifVirement(virement.getMotifVirement());
        virementDto.setMontantVirement(virement.getMontantVirement());
        virementDto.setDateVirement(virement.getDateExecution());
        

        return virementDto;

    }
    public static VersementDto versementToVersementDto(Versement versement) {
 	   versementDto = new VersementDto();
 	   versementDto.setDateVersement(versement.getDateExecution());
 	   versementDto.setNrCompteBeneficiaire(versement.getCompteBeneficiaire().getRib());
 	   versementDto.setMontantVersement(versement.getMontantVersement());
 	   versementDto.setMotifVersement(versement.getMotifVersement());
 	   versementDto.setUsernameEmetteur(versement.getUsernameEmetteur());
 	   
 	   return versementDto;
    }
	/*public static Utilisateur utilisateurDtoToUtilisateur(UtilisateurDto utilisateurDto) {
		utilisateur=new Utilisateur();
		utilisateur.setUsername(utilisateurDto.getUsername());
		utilisateur.setFirstname(utilisateurDto.getFirstname());
		utilisateur.setLastname(utilisateurDto.getLastname());
		utilisateur.setGender(utilisateurDto.getGender());
		utilisateur.setBirthdate(utilisateurDto.getBirthdate());
		return utilisateur;
	}*/
}
