package ma.octo.assignement.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.dto.UtilisateurDto;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;

public interface IntServices {
	
	public void initUtilisteur(UtilisateurDto utilsateurDto);
	public void initCompte(CompteDto compteDto);
	
	public List<VirementDto> loadVirements();
	public List<VersementDto> loadVersements();
	public List<Utilisateur> loadUtilisateurs();
	public List<Compte> loadComptes();
	public void newVirement(VirementDto virementDto) throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException;
	public void newVersement(VersementDto versementDto) throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException;
	
}
