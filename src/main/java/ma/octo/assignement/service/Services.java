package ma.octo.assignement.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.dto.UtilisateurDto;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.repository.VirementRepository;
import ma.octo.assignement.mapper.VirementMapper;

@Service
@Transactional
public class Services implements IntServices{
	public static final int MONTANT_MAXIMAL = 10000;
	public static final int MONTANT_MINIMAL = 10;
	public static final int VIREMENT_MAXIMAL = 10;
	
	AutiService autiService;
	
	CompteRepository compteRepository;
	UtilisateurRepository utilisateurRepository;
	VirementRepository virementRepository;
	VersementRepository versementRepository;
	
	
	public Services(CompteRepository compteRepository,UtilisateurRepository utilisateurRepository,VirementRepository virementRepository,VersementRepository versementRepository,AutiService autiService) {
		this.compteRepository=compteRepository;
		this.utilisateurRepository=utilisateurRepository;
		this.virementRepository=virementRepository;
		this.versementRepository=versementRepository;
		this.autiService=autiService;
		
	}
	Logger LOGGER = LoggerFactory.getLogger(Services.class);

	@Override
	public void newVirement(VirementDto virementDto) throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException  {
		// data depuis virementDto
		Compte compteEmetteur=compteRepository.findByNrCompte(virementDto.getNrCompteEmetteur());
		Compte compteBeneficiaire=compteRepository.findByNrCompte(virementDto.getNrCompteBeneficiaire());
		BigDecimal montantVirement = virementDto.getMontantVirement();
		long virementsCount = virementRepository.count();
		String motifVirement = virementDto.getMotifVirement();
		//les exception sur le data
        if(compteEmetteur==null ) {
        	throw new CompteNonExistantException("Compte Emetteur Non existant");
        }
        else if(compteBeneficiaire==null) {
        	throw new CompteNonExistantException("Compte Beneficiaire Non existant");
        }
        else if(montantVirement.equals(null) ||montantVirement.intValue() == 0) {
        	throw new TransactionException("montant vide");
        }
        else if(montantVirement.intValue() < MONTANT_MINIMAL || montantVirement.intValue() > MONTANT_MAXIMAL) {
        	throw new TransactionException("Montant de virement doit etre inclu entre"+MONTANT_MINIMAL+" et "+MONTANT_MAXIMAL);
        }
        else if(compteEmetteur.getSolde().intValue() - montantVirement.intValue() < 0) {
        	LOGGER.error("Solde insuffisant pour affecté le virement");
        }
        else if(motifVirement.length() < 0) {
			LOGGER.error("le nombre de virement réalisé dépasse 10");
			throw new TransactionException("Motif de virement et vide");
		}
        else if(virementsCount >= VIREMENT_MAXIMAL) {
			throw new TransactionException("tu as depasse le max de virements :"+VIREMENT_MAXIMAL);
		}
        else {
        	//emeteur Compte
        	BigDecimal newEmetteurSolde = compteEmetteur.getSolde().subtract(montantVirement);
    		compteEmetteur.setSolde(newEmetteurSolde);
    		compteRepository.save(compteEmetteur);
    		//Beneficiair Compte
    		BigDecimal newBeneficiaireSolde = new BigDecimal(
    		compteBeneficiaire.getSolde().intValue() + virementDto.getMontantVirement().intValue());
    		compteBeneficiaire.setSolde(newBeneficiaireSolde);
    		compteRepository.save(compteBeneficiaire);
    		//virement
    		//autiService.initVirement(compteEmetteur.getNrCompte(),compteBeneficiaire.getNrCompte(),virementDto.getDateVirement(),null,montantVirement);
    		Virement virement = new Virement();
    		virement.setDateExecution(virementDto.getDateVirement());
    		virement.setCompteBeneficiaire(compteBeneficiaire);
    		virement.setCompteEmetteur(compteEmetteur);
    		virement.setMontantVirement(montantVirement);
    		virementRepository.save(virement);
    		autiService.auditVirement("Virement depuis le compte " + compteEmetteur.getNrCompte() + " vers le compte "
    				+ compteBeneficiaire.getNrCompte() + " d'un montant de " + montantVirement.toString());
        }
		
		
	}

	@Override
	public void newVersement(VersementDto versementDto) throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException  {
		// data depuis virementDto
				Compte compteBeneficiaire=compteRepository.findByNrCompte(versementDto.getNrCompteBeneficiaire());
				String utilisateur=versementDto.getUsernameEmetteur();
				BigDecimal montantVersement = versementDto.getMontantVersement();
				long versementsCount = versementRepository.count();
				String motifVersement = versementDto.getMotifVersement();
				//les exception sur data
				if(utilisateur==null) {
		        	throw new CompteNonExistantException("il faut entre un nom d'utilisateur");
		        }
				else if(compteBeneficiaire==null) {
		        	throw new CompteNonExistantException("Compte Beneficiaire Non existant");
		        }
		        else if(montantVersement.equals(null) || montantVersement.intValue() == 0) {
		        	throw new TransactionException("montant vide");
		        }
		        else if(montantVersement.intValue() < MONTANT_MINIMAL || montantVersement.intValue() > MONTANT_MAXIMAL) {
		        	throw new TransactionException("Montant de virement doit etre inclu entre"+MONTANT_MINIMAL+" et "+MONTANT_MAXIMAL);
		        }
		        else if (motifVersement.length() < 0) {
					throw new TransactionException("Motif vide");
				}
		        else {
		        	//Beneficiair Compte
		    		BigDecimal newBeneficiaireSolde = new BigDecimal(
		    		compteBeneficiaire.getSolde().intValue() + versementDto.getMontantVersement().intValue());
		    		compteBeneficiaire.setSolde(newBeneficiaireSolde);
		    		compteRepository.save(compteBeneficiaire);
		    		//versement
		    		Versement versement = new Versement();
		    		versement.setDateExecution(versementDto.getDateVersement());
		    		versement.setCompteBeneficiaire(compteBeneficiaire);
		    		versement.setUsernameEmetteur(utilisateur);
		    		versement.setMontantVersement(montantVersement);
		    		versementRepository.save(versement);
		    		autiService.auditVersement("Versement depuis l'utilisateur " + utilisateur+ " vers le compte "
		    				+ compteBeneficiaire.getNrCompte() + " d'un montant de " + montantVersement.toString());
		        }
		
	}

	@Override
	public List<VirementDto> loadVirements() {
		List<Virement> virements = virementRepository.findAll();
		List<VirementDto> virementDtos = virements.stream().map(ele -> VirementMapper.virementToVirementDto(ele))
				.collect(Collectors.toList());
		return virementDtos.isEmpty() ? null : virementDtos;
	}

	@Override
	public List<VersementDto> loadVersements() {
		List<Versement> versements = versementRepository.findAll();
		List<VersementDto> versementDtos = versements.stream().map(ele -> VirementMapper.versementToVersementDto(ele))
				.collect(Collectors.toList());
		return versementDtos.isEmpty() ? null : versementDtos;
	}

	@Override
	public List<Utilisateur> loadUtilisateurs() {
		List<Utilisateur> utilisateurs=utilisateurRepository.findAll();
		return utilisateurs.isEmpty() ? null:utilisateurs;
	}

	@Override
	public List<Compte> loadComptes() {
		List<Compte> comptes=compteRepository.findAll();
		return comptes.isEmpty() ? null:comptes;
	}

	@Override
	//public void initUtilisteur(String username, String firstName, String lastName, String gendre, Date birthDay) {
	public void initUtilisteur(UtilisateurDto utilsateurDto) {
	    Utilisateur utilisateur = new Utilisateur();
		utilisateur.setUsername(utilsateurDto.getUsername());
		utilisateur.setLastname(utilsateurDto.getLastname());
		utilisateur.setFirstname(utilsateurDto.getFirstname());
		utilisateur.setGender(utilsateurDto.getGender());
        utilisateur.setBirthdate(utilsateurDto.getBirthdate());
		utilisateurRepository.save(utilisateur);
		
	}

	@Override
	public void initCompte(CompteDto compteDto) {
		Compte compte1 = new Compte();
		compte1.setNrCompte(compteDto.getNrCompte());
		compte1.setRib(compteDto.getRib());
		compte1.setSolde(compteDto.getSolde());
		Utilisateur utilisateur=utilisateurRepository.findByUsername(compteDto.getUsername());
		if(utilisateur==null ) {
			System.out.print("this utilisateur doesn't exist");
		}
		else {
		compte1.setUtilisateur(utilisateur);

		compteRepository.save(compte1);}
		
	}
	
	

}
