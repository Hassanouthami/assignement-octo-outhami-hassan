package ma.octo.assignement;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.dto.UtilisateurDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.repository.AuditVirementRepository;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.repository.VirementRepository;
import ma.octo.assignement.service.AutiService;
import ma.octo.assignement.service.IntServices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.math.BigDecimal;
import java.util.Date;

@SpringBootApplication
public class AssignementApplication implements CommandLineRunner {
	@Autowired
	private IntServices service;
	/*@Autowired
	private CompteRepository compteRepository;
	@Autowired
	private UtilisateurRepository utilisateurRepository;
	@Autowired
	private VirementRepository virementRepository;
    @Autowired
    private AuditVirementRepository auditVirementRepository;*/

	public static void main(String[] args) {
		SpringApplication.run(AssignementApplication.class, args);
	}

	@Override
	public void run(String... strings) throws Exception {
		
		UtilisateurDto utilisateur1=new UtilisateurDto("user1", "last1", "first1", "Male",new Date(28/01/1999));
		UtilisateurDto utilisateur2=new UtilisateurDto("user2", "last2", "first2","Female",new Date(28/01/2000));
		CompteDto compte1=new CompteDto("010000A000001000","RIB1",utilisateur1.getUsername(),BigDecimal.valueOf(200000L));
		CompteDto compte2=new CompteDto("010000B025001000","RIB2",utilisateur2.getUsername(),BigDecimal.valueOf(14000L));
		VirementDto virement=new VirementDto("010000A000001000", "010000B025001000","Assignment 2021",BigDecimal.valueOf(200L),new Date());
		service.initUtilisteur(utilisateur1);
		service.initUtilisteur(utilisateur2);
        service.initCompte(compte1);
	    service.initCompte(compte2);
        service.newVirement(virement);
	
	}
}
