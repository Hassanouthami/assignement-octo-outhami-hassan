package ma.octo.assignement.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.dto.UtilisateurDto;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.VirementMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.repository.VirementRepository;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class ServicesTest {

	public static final int MONTANT_MAXIMAL = 10000;
	public static final int MONTANT_MINIMAL = 10;
	public static final int VIREMENT_MAXIMAL = 10;
	@Autowired
	Services services;
	@Test
	public void testNewVersements() throws Exception {

		VersementDto versementDto = new VersementDto();
		versementDto.setMontantVersement(new BigDecimal(300));
		versementDto.setNrCompteBeneficiaire("3332");
		versementDto.setMotifVersement("Versement");

		Exception exception = assertThrows(CompteNonExistantException.class, () -> {
			services.newVersement(versementDto);
		});

		String expectedMessage = "il faut entre un nom d'utilisateur";

		assertTrue(expectedMessage.equals(exception.getMessage()));
	}
	//test virement
	@Test
	public void testNewVirements() throws Exception {

		VirementDto virementDto = new VirementDto();
		virementDto.setMontantVirement(new BigDecimal(3));
		virementDto.setNrCompteBeneficiaire("010000B025001000");
		virementDto.setNrCompteEmetteur("010000A000001000");
		virementDto.setMotifVirement("Versement");

		Exception exception = assertThrows(TransactionException.class, () -> {
			services.newVirement(virementDto);
		});

		String expectedMessage = "Montant de virement doit etre inclu entre"+MONTANT_MINIMAL+" et "+MONTANT_MAXIMAL;

		assertTrue(expectedMessage.equals(exception.getMessage()));
	}
	




}
